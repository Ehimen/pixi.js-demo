
var KEY_CODES = {
	w: 87,
	a: 65,
	s: 83,
	d: 68,
	sp: 32
};

var moveDistance = 3;

var r = new PIXI.CanvasRenderer(800, 600);

var stage = new PIXI.Stage(0xccccff, true);

document.body.appendChild(r.view);

var bunnyTexture = PIXI.Texture.fromImage("dragon1.png");
var bunny = new PIXI.Sprite(bunnyTexture);

function updateUi()
{
	$('#bunny-speed').html(moveDistance);
}

$('#speed-up').on('click', function() { moveDistance++; updateUi(); });
$('#speed-down').on('click', function() { moveDistance--; updateUi(); });

bunny.position.x = 400;
bunny.position.y = 300;

bunny.pivot = new PIXI.Point(50, 46);
bunny.setInteractive(true);
bunny.scale = new PIXI.Point(0.5, 0.5);
stage.addChild(bunny);

requestAnimationFrame(animate);

scaleFactorX = 1;
scaleFactorY = 1;
bRot = 0;

incrementingAcc = false;

accSpeed = 50;
accAmount = 1;
$(window).keydown(function(e) {
	keysDown[e.keyCode] = true;
	if (e.keyCode === KEY_CODES.sp) {
		e.preventDefault();
	}

});

$(window).keyup(function(e) {
	keysDown[e.keyCode] = false;
});

var keysDown = {};

keysDown[KEY_CODES.w] = false;
keysDown[KEY_CODES.a] = false;
keysDown[KEY_CODES.s] = false;
keysDown[KEY_CODES.d] = false;

missileSpeed = 10;
missiles = [];

function moveMissiles()
{
	for (i in missiles) {
		if (missiles.hasOwnProperty(i)) {
			missiles[i].update && missiles[i].update();
		}
	}
}

function moveBunny()
{
		
	if (keysDown[KEY_CODES.a]) {
		// Left
		bunny.position.x -= moveDistance;

	}
	if (keysDown[KEY_CODES.s]) {
		// Down
		bunny.position.y += moveDistance;

	}
	if (keysDown[KEY_CODES.w]) {
		// Up
		bunny.position.y -= moveDistance;

	}
	if (keysDown[KEY_CODES.d]) {
		// Right
		bunny.position.x += moveDistance;
	}

	if (keysDown[KEY_CODES.sp]) {
		// Space bar. FIRE!
		var head = getBunnyHead();
		var speed = Math.floor(Math.random() * 10) + 1;
		missiles.push(new Missile(speed, bRot - Math.PI, head.x, head.y));
	//	e.preventDefault();
	}
}

function getBunnyCenter() {
	return {
		x: bunny.position.x,
		y: bunny.position.y
	};
}

function getBunnyHead()
{
	return {
		x: bunny.position.x + Math.cos(bRot) * -20 - bunny.width / 4,
		y: bunny.position.y + Math.sin(bRot) * -20 + bunny.height / 8,
	}
}

function animate() {
	bunny.rotation = bRot;
	moveBunny();
	moveMissiles();
	r.render(stage);
	requestAnimationFrame(animate);
}

stage.mousemove = function(i)
{
	var p, rot;
	c = getBunnyCenter();
	i = i.global;
	p = diffPoints(c, i);
	rot = Math.atan2(p.y, p.x);
	bRot = rot + 2 * Math.PI;
}

function diffPoints(p2, p1)
{
	return {x: p2.x - p1.x, y: p2.y - p1.y}
}


var Missile = function(speed, direction, x, y)
{
	var texture = PIXI.Texture.fromImage(this.texture);
	this.sprite = new PIXI.Sprite(texture);
	this.sprite.position.x = x;
	this.sprite.position.y = y;
	this.delta = {
		x: Math.cos(direction) * speed,
		y: Math.sin(direction) * speed
	};
	stage.addChild(this.sprite);
};

Missile.prototype.texture = 'missile.png';
Missile.prototype.update = function()
{
	this.sprite.position.x += this.delta.x;	
	this.sprite.position.y += this.delta.y;	
};

updateUi();
