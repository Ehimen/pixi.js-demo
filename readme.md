Locally
-------
* Clone this repository
* Load index.html in a modern browser

Or Online
------
[Preview](http://aicook.co.uk/pixijs-demo/)

**Check out [pixi.js](http://www.pixijs.com/), or on [Github](https://github.com/GoodBoyDigital/pixi.js)**

**A Disclaimer**
I've included the source for this for completeness only. Yes, it is a global, spaghetti-code mess.
